package service;

import com.fasterxml.jackson.databind.JsonNode;

public interface ProductService {

    String addProductService(JsonNode productData);

    String getAllProductsService();

    String deleteProductService(JsonNode customerData);
}
