package service;

import com.fasterxml.jackson.databind.JsonNode;

public interface CustomerService {

    String getAllCustomerService();

    String addCustomerService(JsonNode customerData);

    String deleteCustomerService(JsonNode customerData);
}
