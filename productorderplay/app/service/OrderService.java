package service;

import com.fasterxml.jackson.databind.JsonNode;

public interface OrderService {

    String placeAnOrderByCustomerService(JsonNode orderData);

    String getOrdersByCustomerIdService(int cus_id);

    String chaneOrderStatusService(JsonNode orderData);
}
