package service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import dao.CustomerDao;
import exceptions.CantDeleteException;
import exceptions.NullFieldException;
import org.apache.log4j.Logger;
import service.CustomerService;

import java.util.List;

public class CustomerServiceImpl implements CustomerService {

    private static Logger logger;

    private CustomerDao customerDao;

    public CustomerServiceImpl() {
        customerDao = CustomerDao.getInstance();
        logger = Logger.getLogger(CustomerServiceImpl.class);
    }

    @Override
    public String getAllCustomerService() {
        List<String> result = customerDao.getAllCustomersDao();
        if (result.size() != 0)
            return String.valueOf(result);
        else{
            logger.info("There is no customers - getAllCustomerService");
            return "There is no customers";
        }
    }

    @Override
    public String addCustomerService(JsonNode customerData) {
        if (havingAllFields(customerData))
            return customerDao.addCustomerDao(customerData);
        else{
            logger.error("Missing Customers fields - addCustomerService");
            throw new NullFieldException("Missing Customers fields");
        }
    }

    @Override
    public String deleteCustomerService(JsonNode customerData) {
        int cus_id = customerData.get("cus_id").asInt();
        if (customerDao.isValidCustomer(cus_id))
            return customerDao.deleteCustomerDao(cus_id);
        else{
            logger.error("Invalid Customers id - deleteCustomerService");
            throw new CantDeleteException("Invalid. Cant delete customer");
        }
    }

    private boolean havingAllFields(JsonNode data) {
        if (data == null)
            return false;
        else if (data.get("name") == null || data.get("address") == null ||
                data.get("age") == null || data.get("activeStatus") == null)
            return false;
        else
            return true;
    }


}
