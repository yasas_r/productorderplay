package service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import dao.CustomerDao;
import dao.OrderDao;
import dao.ProductDao;
import exceptions.*;
import org.apache.log4j.Logger;
import org.bson.Document;
import service.OrderService;

import java.util.List;

public class OrderServiceImpl implements OrderService {

    private OrderDao orderDao;
    private CustomerDao customerDao;
    private ProductDao productDao;

    private static Logger logger;

    public OrderServiceImpl() {
        orderDao = OrderDao.getInstance();
        customerDao = CustomerDao.getInstance();
        productDao = ProductDao.getInstance();
        logger = Logger.getLogger(OrderServiceImpl.class);
    }

    @Override
    public String placeAnOrderByCustomerService(JsonNode orderData) {
        if (validatePlaceAnOrder(orderData)) {
            String result = orderDao.placeAnOrderByCustomerDao(orderData);
            productDecrease(orderData); //decrease product stockQuantity
            return result;
        }
        return null;
    }

    @Override
    public String getOrdersByCustomerIdService(int cus_id) {
        logger.debug("This is debug message getOrdersByCustomerIdService");
        List<String> result = orderDao.getOrdersByCustomerIdDao(cus_id);
        if (result.size() != 0)
            return String.valueOf(result);
        else{
            logger.info("There is no Orders - getOrdersByCustomerIdService");
            return "There is no Orders for You";
        }
    }

    @Override
    public String chaneOrderStatusService(JsonNode orderData) {
        if (validateChangingOrderStatus(orderData)) {
            if (isCancelling(orderData)){ //decrease product stockQuantity if and only if -> cancelled
                logger.info("decrease product stockQuantity change status to-> cancelled");
                productIncrease(orderData);
            }
            return orderDao.chaneOrderStatusDao(orderData);
        } else{
            logger.error("Cant change order status - chaneOrderStatusService");
            throw new CantChangeOrderStatusException("Cant change order status");
        }
    }

    private void productDecrease(JsonNode orderData) {
        int productsLength = orderData.get("products").size();
        int productId;
        Double quantity;
        for (int i = 0; i < productsLength; i++) {
            productId = orderData.get("products").get(i).get("productId").asInt();
            quantity = orderData.get("products").get(i).get("quantity").asDouble();
            productDao.decreaseProductStockQuantity(productId, quantity);
        }
    }

    private void productIncrease(JsonNode orderData) {
        Document orderDoc = orderDao.findOrderDoc(orderData.get("order_id").asInt());
        ObjectMapper mapper = new ObjectMapper();
        JsonNode orderNode = null;
        try {
            orderNode = mapper.readTree(orderDoc.toJson());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        int productsLength = orderNode.get("products").size();
        int productId;
        Double quantity;
        for (int i = 0; i < productsLength; i++) {
            productId = orderNode.get("products").get(i).get("productId").asInt();
            quantity = orderNode.get("products").get(i).get("quantity").asDouble();
            productDao.increaseProductStockQuantity(productId, quantity);
        }
    }

    private boolean validateChangingOrderStatus(JsonNode orderData) {
        int order_id = orderData.get("order_id").asInt();
        String newOrderStatus = orderData.get("orderStatus").asText();
        Document productDoc = orderDao.findOrderDoc(order_id);
        String currentOrderStatus = String.valueOf(productDoc.get("orderStatus"));
        if (currentOrderStatus.equals("Pending")) {
            return (newOrderStatus.equals("InProgress") || newOrderStatus.equals("Cancelled")) ? true : false;
        } else if (currentOrderStatus.equals("InProgress")) {
            return (newOrderStatus.equals("Delivered") || newOrderStatus.equals("Cancelled")) ? true : false;
        } else
            return false;
    }

    private boolean isCancelling(JsonNode orderData) {
        return (orderData.get("orderStatus").asText().equals("Cancelled")) ? true : false;
    }

    private boolean validatePlaceAnOrder(JsonNode orderData) {
        if (!havingAllFields(orderData)){
            logger.warn("Missing Ordering Details - validatePlaceAnOrder");
            throw new NullFieldException("Missing Ordering Details");
        }
        else if (!customerDao.isValidAndActiveCustomer(orderData.get("cus_id").asInt())){
            logger.warn("Not Exist or Not a Active Customer - validatePlaceAnOrder");
            throw new NotExistAndActiveCustomerException("Not Exist or Not a Active Customer");
        }
        else if (!isValidAndAvailableInStock(orderData)){
            logger.error("Invalid or UnAvailable products - validatePlaceAnOrder");
            throw new InValidOrNotAvailableException("Invalid or UnAvailable products");
        }
        else if (!isOrderStatusValid(orderData)){
            logger.error("Invalid initial order status - validatePlaceAnOrder");
            throw new InValidOrderStatusException("Invalid order status");
        }
        else
            return true;
    }

    private boolean isValidAndAvailableInStock(JsonNode orderData) {
        int productsLength = orderData.get("products").size();
        int productId;
        int requestQuantity;
        Double availableQuantity;
        for (int i = 0; i < productsLength; i++) {
            productId = orderData.get("products").get(i).get("productId").asInt();
            if (!productDao.isValidProduct(productId)){ //product validity checking
                logger.error("Invalid product order - isValidAndAvailableInStock");
                return false;
            }
            requestQuantity = orderData.get("products").get(i).get("quantity").asInt();
            availableQuantity = productDao.getProductStockQuantityById(productId);
            if (availableQuantity < requestQuantity){ //product availability checking
                logger.error("Unavailable product order - isValidAndAvailableInStock");
                return false;
            }
        }
        return true;
    }

    private boolean isOrderStatusValid(JsonNode data) {
        String orderStatus = data.get("orderStatus").asText();
        return (orderStatus.equals("Pending")) ? true : false;
    }

    private boolean havingAllFields(JsonNode data) {
        if (data == null)
            return false;
        else if (data.get("cus_id") == null || data.get("orderStatus") == null ||
                !isOkayOrderedProductsList(data))
            return false;
        else
            return true;
    }

    private boolean isOkayOrderedProductsList(JsonNode data) {
        int productsLength = data.get("products").size();
        if (productsLength == 0)
            return false;
        else {
            for (int i = 0; i < productsLength; i++) {
                if (data.get("products").get(i).get("productId") == null ||
                        data.get("products").get(i).get("quantity") == null){
                    logger.warn("Invalid order product list - isOkayOrderedProductsList");
                    return false;
                }
            }
        }
        return true;
    }

}
