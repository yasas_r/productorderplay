package service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import dao.ProductDao;
import exceptions.CantDeleteException;
import exceptions.NullFieldException;
import service.ProductService;
import java.util.List;

import org.apache.log4j.Logger;

public class ProductServiceImpl implements ProductService {

    private static Logger logger;

    private ProductDao productDao;

    public ProductServiceImpl() {
        productDao = ProductDao.getInstance();
        logger = Logger.getLogger(ProductServiceImpl.class);
    }

    @Override
    public String addProductService(JsonNode productData) {
        if (havingAllFields(productData))
            return productDao.addProductDao(productData);
        else{
            logger.fatal("This is fatal message getAllProductsService");
            throw new NullFieldException("Missing Products fields");
        }
    }

    @Override
    public String getAllProductsService() {
        logger.debug("This is debug message getAllProductsService");
        logger.fatal("This is fatal message getAllProductsService");
        List<String> result = productDao.getAllProductsDao();
        if (result.size() != 0)
            return String.valueOf(result);
        else{
            logger.info("There is no any products to display - getAllProductsService");
            return "There is no any products";
        }
    }

    @Override
    public String deleteProductService(JsonNode productData) {
        int product_id = productData.get("product_id").asInt();
        if (productDao.isValidProduct(product_id))
            return productDao.deleteProductDao(product_id);
        else{
            logger.error("Invalid product to delete - getAllProductsService");
            throw new CantDeleteException("Invalid. Cant delete Product");
        }
    }

    private boolean havingAllFields(JsonNode data) {
        if (data == null)
            return false;
        else if (data.get("productName") == null || data.get("stockQuantity") == null ||
                data.get("pricePerUnit") == null)
            return false;
        else
            return true;
    }
}
