package bo;

public class Address {
    private String addrline1;
    private String addrline2;
    private String streetNo;
    private String landmark;
    private String city;
    private String country;
    private int zipCode;

    public Address() {
    }

    public Address(String line1, String line2, String streetNo, String landmark, String city, String country, int zipCode) {
        this.addrline1 = line1;
        this.addrline2 = line2;
        this.streetNo = streetNo;
        this.landmark = landmark;
        this.city = city;
        this.country = country;
        this.zipCode = zipCode;
    }

    public void setAddrLine1(String addrline1) {
        this.addrline1 = addrline1;
    }

    public void setAddrLine2(String addrline2) {
        this.addrline2 = addrline2;
    }

    public void setStreetNo(String streetNo) {
        this.streetNo = streetNo;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public String getAddrLine1() {
        return addrline1;
    }

    public String getAddrLine2() {
        return addrline2;
    }

    public String getStreetNo() {
        return streetNo;
    }

    public String getLandmark() {
        return landmark;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public int getZipCode() {
        return zipCode;
    }
}
