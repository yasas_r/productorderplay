package bo;


public class Customer {

    private int cus_id;
    private String name;
    private Address address;
    private int age;
    private boolean activeStatus;

    public Customer() {
    }

    public Customer(int cus_id, String name, Address address, int age, boolean activeStatus) {
        this.cus_id = cus_id;
        this.name = name;
        this.address = address;
        this.age = age;
        this.activeStatus = activeStatus;
    }

    public void setCus_id(int cus_id) {
        this.cus_id = cus_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setActiveStatus(boolean activeStatus) {
        this.activeStatus = activeStatus;
    }

    public int getCus_id() {
        return cus_id;
    }

    public String getName() {
        return name;
    }

    public Address getAddress() {
        return address;
    }

    public int getAge() {
        return age;
    }

    public boolean getActiveStatus() {
        return activeStatus;
    }
}
