package bo;

public class Product {

    private int product_id;
    private String productName;
    private int stockQuantity;
    private int pricePerUnit;

    public Product() {
    }

    public Product(int product_id, String productidName, int stockQuantity, int pricePerUnit) {
        this.product_id = product_id;
        this.productName = productidName;
        this.stockQuantity = stockQuantity;
        this.pricePerUnit = pricePerUnit;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public void setPricePerUnit(int pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public int getProduct_id() {
        return product_id;
    }

    public String getProductName() {
        return productName;
    }

    public int getStockQuantity() {
        return stockQuantity;
    }

    public int getPricePerUnit() {
        return pricePerUnit;
    }
}
