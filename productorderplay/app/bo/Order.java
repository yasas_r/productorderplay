package bo;

import java.util.*;

public class Order {

    private int order_id;
    private List<Map> products;
    private String date;
    private int cus_id;
    private String orderStatus;

    public Order() {
        this.products = new ArrayList<Map>();
    }

    public Order(int order_id, ArrayList<Map> products, String date, int cus_id, String orderStatus) {
        this.order_id = order_id;
        this.products = products;
        this.date = date;
        this.cus_id = cus_id;
        this.orderStatus = orderStatus;
    }

    public void addProductToList(int productId, int quantity) {
        Map map = new LinkedHashMap();
        map.put("productId", productId);
        map.put("quantity", quantity);
        this.products.add(map);
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public void setProducts(List<Map> products) {
        this.products = products;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setCus_id(int cus_id) {
        this.cus_id = cus_id;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public int getOrder_id() {
        return order_id;
    }

    public List<Map> getProducts() {
        return products;
    }

    public String getDate() {
        return date;
    }

    public int getCus_id() {
        return cus_id;
    }

    public String getOrderStatus() {
        return orderStatus;
    }
}
