package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.mvc.*;
import service.impl.ProductServiceImpl;

public class ProductController extends Controller {

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */

    ProductServiceImpl productServiceImpl;

    public ProductController() {
        this.productServiceImpl = new ProductServiceImpl();
    }

    public Result index() {
        return ok(views.html.index.render());
    }

    public Result addProduct(Http.Request request) {
        JsonNode requestData = request.body().asJson();
        return ok(productServiceImpl.addProductService(requestData));
    }

    public Result getAllProducts() {
        return ok(productServiceImpl.getAllProductsService());
    }

    public Result deleteProduct(Http.Request request) {
        JsonNode requestData = request.body().asJson();
        return ok(productServiceImpl.deleteProductService(requestData));
    }


}