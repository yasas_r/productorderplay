package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.mvc.*;
import service.impl.OrderServiceImpl;

public class OrderController extends Controller {

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    OrderServiceImpl orderServiceImpl;

    public OrderController() {
        this.orderServiceImpl = new OrderServiceImpl();
    }

    public Result index() {
        return ok(views.html.index.render());
    }

    public Result placeAnOrderByCustomer(Http.Request request) {
        JsonNode requestData = request.body().asJson();
        return ok(orderServiceImpl.placeAnOrderByCustomerService(requestData));
    }

    public Result getOrdersByCustomerId(String cus_id) {
        return ok(orderServiceImpl.getOrdersByCustomerIdService(Integer.parseInt(cus_id)));
    }

    public Result chaneOrderStatus(Http.Request request) {
        JsonNode requestData = request.body().asJson();
        return ok(orderServiceImpl.chaneOrderStatusService(requestData));
    }

}
