package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.mvc.*;
import service.impl.CustomerServiceImpl;

public class CustomerController extends Controller {

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    CustomerServiceImpl customerServiceImpl;

    public CustomerController() {
        this.customerServiceImpl = new CustomerServiceImpl();
        ;
    }

    public Result index() {
        return ok(views.html.index.render());
    }

    public Result addCustomer(Http.Request request) {
        JsonNode requestData = request.body().asJson();
        return ok(customerServiceImpl.addCustomerService(requestData));
    }

    public Result getAllCustomer() {
        return ok(customerServiceImpl.getAllCustomerService());
    }

    public Result deleteCustomer(Http.Request request) {
        JsonNode requestData = request.body().asJson();
        return ok(customerServiceImpl.deleteCustomerService(requestData));
    }

}
