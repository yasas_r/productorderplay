package dao.connection;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

public class MongoConnection {

    private static MongoClient mongoClient;

    public static MongoDatabase getDatabase() {
        mongoClient = new MongoClient();
        return mongoClient.getDatabase("ncinga_training");
    }
}
