package dao;

import bo.Address;
import bo.Customer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import dao.connection.MongoConnection;
import org.bson.Document;
import play.libs.Json;

import java.util.ArrayList;
import java.util.List;

public class CustomerDao {

    private static CustomerDao customerDao;

    private MongoDatabase database;
    private MongoCollection<Document> customersCollection;

    public CustomerDao() {
        this.database = MongoConnection.getDatabase();
        customersCollection = database.getCollection("customers");
    }

    public static CustomerDao getInstance() {
        customerDao = customerDao == null ? new CustomerDao() : customerDao;
        return customerDao;
    }

    public String addCustomerDao(JsonNode customerData) {
        Customer customer = createCustomerObj(customerData);
        JsonNode customerJsNode = Json.toJson(customer);
        customersCollection.insertOne(Document.parse(String.valueOf(customerJsNode)));
        return customer.getName() + " is added";
    }

    public List<String> getAllCustomersDao() {
        MongoCursor<Document> cursor = customersCollection.find().iterator();
        List<String> resultSet = new ArrayList<>();
        while (cursor.hasNext()) {
            resultSet.add(cursor.next().toJson());
        }
        cursor.close();
        return resultSet;
    }

    public String deleteCustomerDao(int cus_id) {
        Document customerDoc = customersCollection.findOneAndDelete(new Document("cus_id", cus_id));
        return customerDoc.get("name") + " is deleted";
    }

    public Document findCustomerDoc(int cus_id) {
        return customersCollection.find(new Document("cus_id", cus_id)).first();
    }

    public boolean isValidCustomer(int cus_id) {
        Document customerDoc = findCustomerDoc(cus_id);
        return customerDoc == null ? false : true;
    }

    public boolean isValidAndActiveCustomer(int cus_id) {
        Document customerDoc = findCustomerDoc(cus_id);
        if (customerDoc == null)
            return false;
        else if ((boolean) customerDoc.get("activeStatus") == false)
            return false;
        else
            return true;
    }

    public Customer findCustomerById(int cus_id) {
        Document customerDoc = customersCollection.find(new Document("cus_id", cus_id)).first();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = null;
        try {
            node = mapper.readTree(customerDoc.toJson());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        Customer customer = createCustomerObj(node);
        return customer;
    }

    private Customer createCustomerObj(JsonNode customerData) {
        Customer customer = new Customer();
        Document document = customersCollection.find().sort(new Document("_id", -1)).first();
        int lastCus_id = (int) document.get("cus_id");
        customer.setCus_id(lastCus_id + 1);
        customer.setName(customerData.get("name").asText());
        customer.setAge(customerData.get("age").asInt());
        customer.setActiveStatus(customerData.get("activeStatus").asBoolean());

        Address address = new Address();
        address.setAddrLine1(customerData.get("address").get("addrline1").asText());
        address.setAddrLine2(customerData.get("address").get("addrline2").asText());
        address.setStreetNo(customerData.get("address").get("streeNo").asText());
        address.setLandmark(customerData.get("address").get("landmark").asText());
        address.setCity(customerData.get("address").get("city").asText());
        address.setCountry(customerData.get("address").get("country").asText());
        address.setZipCode(customerData.get("address").get("zipCode").asInt());
        customer.setAddress(address);

        return customer;
    }

}
