package dao;

import bo.Product;
import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import dao.connection.MongoConnection;
import org.bson.Document;
import play.libs.Json;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;

public class ProductDao {

    private static ProductDao productDao;

    private MongoDatabase database;
    private MongoCollection<Document> productsCollection;

    public ProductDao() {
        this.database = MongoConnection.getDatabase();
        productsCollection = database.getCollection("products");
    }

    public static ProductDao getInstance() {
        productDao = productDao == null ? new ProductDao() : productDao;
        return productDao;
    }

    public String addProductDao(JsonNode customerData) {
        Product product = createProductObj(customerData);
        JsonNode customerJsNode = Json.toJson(product);
        productsCollection.insertOne(Document.parse(String.valueOf(customerJsNode)));
        return product.getProductName() + " is added";
    }

    public List<String> getAllProductsDao() {
        MongoCursor<Document> cursor = productsCollection.find().iterator();
        List<String> resultSet = new ArrayList<>();
        while (cursor.hasNext()) {
            resultSet.add(cursor.next().toJson());
        }
        cursor.close();
        return resultSet;
    }

    public String deleteProductDao(int product_id) {
        Document productDoc = productsCollection.findOneAndDelete(new Document("product_id", product_id));
        return productDoc.get("productName") + " is deleted";
    }

    public boolean isValidProduct(int product_id) {
        Document productDoc = productsCollection.find(new Document("product_id", product_id)).first();
        return productDoc == null ? false : true;
    }

    public Double getProductStockQuantityById(int product_id) {
        Document productDoc = productsCollection.find(new Document("product_id", product_id)).first();
        return (Double) productDoc.get("stockQuantity");
    }

    public Document findProductDoc(int product_id) {
        return productsCollection.find(new Document("product_id", product_id)).first();
    }

    public void decreaseProductStockQuantity(int product_id, Double orderQuantity) {
        Document productDoc = findProductDoc(product_id);
        Double availableQuantity = (Double) productDoc.get("stockQuantity");
        Double newQuantity = availableQuantity - orderQuantity;
        productsCollection.updateOne(eq("product_id", product_id), new Document("$set", new Document("stockQuantity", newQuantity)));
    }

    public void increaseProductStockQuantity(int product_id, Double orderQuantity) {
        Document productDoc = findProductDoc(product_id);
        Double availableQuantity = (Double) productDoc.get("stockQuantity");
        Double newQuantity = availableQuantity + orderQuantity;
        productsCollection.updateOne(eq("product_id", product_id), new Document("$set", new Document("stockQuantity", newQuantity)));
    }

    private Product createProductObj(JsonNode productData) {
        Product product = new Product();
        Document document = productsCollection.find().sort(new Document("_id", -1)).first();
        int lastProduct_id = (int) document.get("order_id");
        product.setProduct_id(lastProduct_id + 1);
        product.setProductName(productData.get("productName").asText());
        product.setStockQuantity(productData.get("stockQuantity").asInt());
        product.setPricePerUnit(productData.get("pricePerUnit").asInt());
        return product;
    }

}
