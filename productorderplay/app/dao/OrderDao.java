package dao;

import bo.Order;
import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import dao.connection.MongoConnection;
import org.bson.Document;
import play.libs.Json;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;

public class OrderDao {

    private static OrderDao orderDao;

    private MongoDatabase database;
    private MongoCollection<Document> ordersCollection;

    public OrderDao() {
        this.database = MongoConnection.getDatabase();
        ordersCollection = database.getCollection("orders");
    }

    public static OrderDao getInstance() {
        orderDao = orderDao == null ? new OrderDao() : orderDao;
        return orderDao;
    }

    public String placeAnOrderByCustomerDao(JsonNode orderData) {
        Order order = createOrderObj(orderData);
        JsonNode orderJsNode = Json.toJson(order);
        ordersCollection.insertOne(Document.parse(String.valueOf(orderJsNode)));
        return "Order id " + order.getOrder_id() + " is added -- order status is " + order.getOrderStatus();
    }

    public List<String> getOrdersByCustomerIdDao(int cus_id) {
        MongoCursor<Document> cursor = ordersCollection.find(new Document("cus_id", cus_id)).iterator();
        List<String> resultSet = new ArrayList<>();
        while (cursor.hasNext()) {
            resultSet.add(cursor.next().toJson());
        }
        cursor.close();
        return resultSet;
    }

    public String chaneOrderStatusDao(JsonNode orderData) {
        int order_id = orderData.get("order_id").asInt();
        String orderStatus = orderData.get("orderStatus").asText();
        ordersCollection.updateOne(eq("order_id", order_id), new Document("$set", new Document("orderStatus", orderStatus)));
        return "changed Order status to " + orderStatus;
    }

    public Document findOrderDoc(int order_id) {
        return ordersCollection.find(new Document("order_id", order_id)).first();
    }

    private Order createOrderObj(JsonNode orderData) {
        Order order = new Order();
        Document document = ordersCollection.find().sort(new Document("_id", -1)).first();
        int lastOrder_id = (int) document.get("order_id");
        order.setOrder_id(lastOrder_id + 1);
        int productsLength = orderData.get("products").size();
        int productId;
        int quantity;
        for (int i = 0; i < productsLength; i++) {
            productId = orderData.get("products").get(i).get("productId").asInt();
            quantity = orderData.get("products").get(i).get("quantity").asInt();
            order.addProductToList(productId, quantity);
        }
        order.setCus_id(orderData.get("cus_id").asInt());
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        order.setDate(dateFormat.format(date));
        order.setOrderStatus(orderData.get("orderStatus").asText());
        return order;
    }
}
