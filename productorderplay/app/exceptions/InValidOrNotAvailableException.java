package exceptions;

public class InValidOrNotAvailableException extends RuntimeException{

    public InValidOrNotAvailableException(String message){
        super(message);
    }
}
