package exceptions;

public class CantChangeOrderStatusException extends RuntimeException{

    public CantChangeOrderStatusException(String message){
        super(message);
    }
}