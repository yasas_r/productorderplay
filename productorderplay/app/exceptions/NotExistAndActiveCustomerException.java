package exceptions;

public class NotExistAndActiveCustomerException extends RuntimeException{

    public NotExistAndActiveCustomerException(String message){
        super(message);
    }
}
