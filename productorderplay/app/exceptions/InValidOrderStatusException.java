package exceptions;

public class InValidOrderStatusException extends RuntimeException{

    public InValidOrderStatusException(String message){
        super(message);
    }
}
